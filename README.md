# MartServer Setup
This project provides the artifacts required to setup the OCCI API utilized for the runtime workflow modeling [case studies](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples). It contains of a compiled version of the [MartServer](https://github.com/occiware/MartServer), as well as required [plugins](./martserver-plugins) and [configuration management scripts](./cm). In the following a getting started guide is provided that allows you to quickly test the capabilities of [DOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci), a models at runtime engine adapting cloud application topologies, and [WOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.wocci) enacting workflows with arbitrary infrastructure requirements modeled as OCCI configuration. Moreover, this project provides a guide on how to setup the MartServer for the utilization in an actual cloud environment.

## Getting started
To get started just clone the repository. We already configured the MartServer in such a way that it works out of the box for testing purposes.
Navigate to the root folder and execute the following command:
```
java -jar org.occiware.mart.jetty.jar
```

Now the MartServer should be started and several extensions should be loaded. The log output about the loaded OCCI extensions should look similar to the following:
```
INFO Collection: [http://schemas.modmacao.org/occi/platform#, http://schemas.ogf.org/occi/infrastructure/compute/template/1.1#, http://schemas.modmacao.org/mongodb#, http://schemas.modmacao.org/sugarcrm#, http://schemas.modmacao.org/openstack/runtime#, http://schemas.modmacao.org/modmacao#, http://schemas.ogf.org/occi/sla#, http://schemas.ugoe.cs.rwm/workflow#, http://schemas.modmacao.org/toscabasetypes#, http://schemas.ogf.org/occi/core#, http://schemas.modmacao.org/toscaspecifictypes#, http://schemas.ogf.org/tosca/core#, http://schemas.modmacao.org/placement#, http://schemas.modmacao.org/lamp#, http://schemas.ogf.org/occi/infrastructure#, http://schemas.modmacao.org/occi/ansible#, http://schemas.modmacao.org/openstack/swe#, http://schemas.ogf.org/tosca/extended#, http://schemas.ugoe.cs.rwm/monitoring#]
```

You successfully started a MartServer including multiple extensions.
Now you can send OCCI requests to the MartServer to deploy dummy cloud applications.
These applications are not actually deployed and are only represented within the intern runtime model of the MartServer.

### Testing the MartServer
To test whether the server is running, try the following request. To perform the request command line tools like curl can be used.
```
curl -v -X GET http://localhost:8080/.well-known/org/ogf/occi/-/ -H "accept: application/json" -u admin:1234 
```
After sending this request you should receive an enumeration of multiple OCCI Kinds that are registered within the MartServer.

To create a Virtual Machine the following request can be send.
This request asks for the creation of a Compute node. However, as currently only connector dummies are utilized for testing purposes the requested Virtual Machine is only present in the runtime model of the MartServer.
To actually create Virtual Machines within a cloud environment follow the steps described in the [platform specific configurations](/doc/MartServer.md) of the MartServer.

```
curl -v -X PUT http://localhost:8080/compute/urn:uuid:15462ca5-5b29-4df6-a899-f95e497a2947 -H 'Content-Type: text/occi' -H 'Category: compute; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind"' -H 'X-OCCI-Attribute: occi.core.id="urn:uuid:15462ca5-5b29-4df6-a899-f95e497a2947"' -H 'X-OCCI-Attribute: occi.core.title="TestVM"'
```
After sending this request you get a response that your Compute node was successfully created. You can even inspect running resources using your browser.
E.g., you can visit [localhost:8080/compute](localhost:8080/compute) to gain information about all provisioned Compute nodes.

## SmartWYRM
To visualize the provisioned and deployed OCCI cloud application, [SmartWYRM](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.smartwyrm) can be used.
SmarWYRM is a server application that allows to easily adapt cloud applications and perform scientific workflows based on the MartServer setup presented in this project.
It is easy to deploy and provides a graphical interface for the models at runtime engine [DOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci), as well as the workflow engine [WOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.wocci). Moreover, it provides an interface to perform simple model to model transformations using [TOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci). For example, platform independent OCCI cloud configurations can be enhanced with platform specific information required to perform an actual cloud deployment. This can comprise the addition of ssh keys and user data to be used by each Virtual Machine, as well as required model setups to execute configuration management scripts. 

To utilize the benefits of [SmartWYRM](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.smartwyrm) simply follow the description below:
1. Download the [latest build](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.smartwyrm/-/jobs/artifacts/master/download?job=build) of SmartWYRM.
2. Extract the smartwyrm.jar and put it into the mart.setup repository folder next to the martserver.jar.
3. Navigate to the folder and start the smartwyrm.jar
```
java -jar smartwyrm.jar
```
### Testing SmartWYRM
Now smartwyrm is started and should be reachable at [localhost:8081](localhost:8081).

To test [SmartWYRM's DOCCI](localhost:8081/docci) just download an [example OCCI cloud application](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/tree/master/docci) configuration and upload, transform, and deploy it using SmartWYRM.

To test [SmartWYRM's WOCCI](localhost:8081/wocci) download an [example workflow](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/tree/master/docci) and enact it using SmartWYRM.

*Note:* These tests are only performed for testing purposes provisioning and deploying the OCCI elements on a model based level.

## MartServer cloud configuration
To setup the MartServer to forward incoming requests to cloud some of configurations have be performed.
Therefore, we provide a brief explanation of the different components used within the presented MartServer configuration in the following.
Moreover, we describe how the individual components of the MartServer has to be configured to allow for the replication of the runtime workflow modeling case studies.
The required configuration steps, as well as the description of used components, can be found [here](/doc/MartServer.md).