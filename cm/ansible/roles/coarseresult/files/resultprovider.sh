#! /bin/bash

endpoint=$1
propresource=$2
propname=$3
sensor=$4
propid=$5
thresholdPicking=$6
monfile=/opt/mls/factoryExample/log
sequence=3

echo "Endpoint: $endpoint"
echo "Resource: $propresource"
echo "Name: $propname"
echo "Picking Threshold: $thresholdPicking"

echo "Starting publishing script"
echo "Checking $monfile every $sequence seconds with threshold of $thresholdPicking!"


while true; do
    val=$(tail -5 "$monfile" | head -n 1 | cut -c 13-)
    echo "$val"

    if [ $(echo "$val > $thresholdPicking" | bc ) = 1 ]; then
       state=picking
    else
       state=qa
    fi
    

    if [ "$oldstate" = "$state" ]; then
      echo "Nothing changed! Still in state $state"
    else
      echo "New state reached: $state"
      echo "Publishing new results"
      curl -v -X PUT http://"$endpoint""$propid" -H 'Content-Type: text/occi'  -H 'Category: monitorableproperty; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="'$sensor'",occi.core.target="'$propresource'", occi.core.title="monProp", monitoring.result="'$state'", monitoring.property="'$propname'"' 
      oldstate=$state
    fi
    sleep $sequence
done

