#! /bin/bash

endpoint=$1
propresource=$2
propname=$3
sensor=$4
propid=$5
thresholdCrit=90
thresholdHigh=60
thresholdMid=40
thresholdLow=10
monfile=~/monitor.txt
sequence=3

echo "Endpoint: $endpoint"
echo "Resource: $propresource"
echo "Name: $propname"

if [! -f "$monfile" ]; then
    echo "Did not find file for monitoring: $monfile"
    sleep 10
fi

if [! -f "$monfile" ]; then
    echo "Did not find file for monitoring: $monfile"
    sleep 10
fi

echo "Starting publishing script"
echo "Checking $monfile every $sequence seconds with threshold of $threshold!"


while true; do
    val=$(tail -1 $monfile)
    echo "$val"

    if [ $(echo "$val > $thresholdCrit" | bc ) = 1 ]; then
       state=Critical
    elif [ $(echo "$val > $thresholdHigh" | bc ) = 1 ]; then
       state=High
    elif [ $(echo "$val > $thresholdMid" | bc ) = 1 ]; then
       state=Mid
    elif [ $(echo "$val > $thresholdLow" | bc ) = 1 ]; then
       state=Low
    else
       state=None
    fi
    

    if [ "$oldstate" = "$state" ]; then
      echo "Nothing changed! Still in state $state"
    else
      echo "New state reached: $state"
      echo "Publishing new results"
      curl -v -X PUT http://"$endpoint""$propid" -H 'Content-Type: text/occi'  -H 'Category: monitorableproperty; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="'$sensor'",occi.core.target="'$propresource'", occi.core.title="monProp", monitoring.result="'$state'", monitoring.property="'$propname'"' 
      oldstate=$state
    fi
    sleep $sequence
done

