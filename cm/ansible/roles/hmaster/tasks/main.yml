---
# tasks file for simApp
- block:
  - name: enable ubuntu group
    group: name=ubuntu state=present

  - name: enable ubuntu user
    user: name=ubuntu state=present shell=/bin/bash

  - name: make sure AllowUser statement is absent
    lineinfile:
      dest: /etc/ssh/sshd_config
      regexp: '^AllowUsers*'
      state: absent

  - name: Install Java Package
    apt: pkg=openjdk-8-jre-headless state=installed update_cache=true force=yes

  - name: Creating hadoop folder
    file: path=/opt state=directory

  - name: Upload hadoop
    unarchive:
      src: hadoop-2.9.2.tar.gz
      dest: /opt

  - name: make sure hadoop folder has the correct permissions
    file: path=/opt/hadoop-2.9.2 owner=ubuntu group=ubuntu recurse=yes

  - name: make sure sshd is restarted
    service: name=ssh state=restarted

  when: task == "DEPLOY"
  remote_user: ubuntu

- block:
  - name: edit core-site.xml configuration 
    shell: echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n
                 <?xml-stylesheet type=\"text/xsl\" href=\"configuration.xsl\"?>\n
                 <configuration>\n
                  \t<property>\n
                  \t\t<name>fs.defaultFS</name>\n
                  \t\t<value>hdfs://hadoop-master:9000</value>\n
                  \t</property>\n
                 </configuration>\n" > /opt/hadoop-2.9.2/etc/hadoop/core-site.xml
  
  - name: edit hdfs-site.xml configuration
    shell: echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                 <?xml-stylesheet type=\"text/xsl\" href=\"configuration.xsl\"?>
                 <configuration>
                   <property>
                     <name>dfs.replication</name>
                     <value>3</value>
                   </property>
                 </configuration>" > /opt/hadoop-2.9.2/etc/hadoop/hdfs-site.xml

  - name: edit mapred-site.xml configuration
    shell: echo "<?xml version=\"1.0\"?>
                 <configuration>
                   <property>
                     <name>mapreduce.framework.name</name>
                     <value>yarn</value>
                   </property>
                 </configuration>" > /opt/hadoop-2.9.2/etc/hadoop/mapred-site.xml

  - name: edit yarn-site.xml configuration
    shell: echo "<?xml version=\"1.0\"?>
                   <configuration>
                     <property>
                       <name>yarn.nodemanager.aux-services</name>
                       <value>mapreduce_shuffle</value>
                     </property>
                     <property>
                       <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
                       <value>org.apache.hadoop.mapred.ShuffleHandler</value>
                     </property>
                     <property>
                       <name>yarn.resourcemanager.resource-tracker.address</name>
                       <value>hadoop-master:8025</value>
                     </property>
                     <property>
                       <name>yarn.resourcemanager.scheduler.address</name>
                       <value>hadoop-master:8030</value>
                     </property>
                     <property>
                       <name>yarn.resourcemanager.address</name>
                       <value>hadoop-master:8040</value>
                     </property>
                  </configuration>" > /opt/hadoop-2.9.2/etc/hadoop/yarn-site.xml

  - name: copy hostfile to cluster instances
    copy: src=hosts dest=/etc/hosts

  - name: set java home
    lineinfile: dest=/opt/hadoop-2.9.2/etc/hadoop/hadoop-env.sh regexp="^export JAVA_HOME=" line="export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre"
  
  - name: make sure .ssh folder is present
    file: dest=/home/ubuntu/.ssh state=directory owner=ubuntu group=ubuntu mode=700

  - name: cp authorized_keys
    copy: src=authorized_keys dest=/home/ubuntu/.ssh/authorized_keys owner=ubuntu group=ubuntu mode=600

  - name: cp id_dsa
    copy: src=id_dsa dest=/home/ubuntu/.ssh/id_dsa owner=ubuntu group=ubuntu mode=600

  - name: cp ssh config
    copy: src=config dest=/home/ubuntu/.ssh/config owner=ubuntu group=ubuntu mode=600

  - name: set path variable for user ubuntu
    lineinfile: dest=/home/ubuntu/.bashrc state=present line="export PATH=$PATH:/opt/hadoop-2.9.2/bin"

  - name: final configuration on master
    copy: src=slaves dest=/opt/hadoop-2.9.2/etc/hadoop/slaves 

  when: task == "CONFIGURE"
  remote_user: ubuntu
 
- block:
  - name: Format HDFS
    shell: yes "yes" | /opt/hadoop-2.9.2/bin/hdfs namenode -format
  - name: Start HDFS
    shell: yes "yes" | /opt/hadoop-2.9.2/sbin/hadoop-daemon.sh start namenode
  - name: Start Yarn
    shell: yes "yes" | /opt/hadoop-2.9.2/sbin/start-yarn.sh
  when: task == "START"
  become_user: "ubuntu"
