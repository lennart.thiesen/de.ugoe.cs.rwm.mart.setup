#! /bin/bash

endpoint=$1
propresource=$2
propname=$3
sensor=$4
propid=$5
sequence=3

echo "Endpoint: $endpoint"
echo "Resource: $propresource"
echo "Name: $propname"

echo "Starting publishing script"

while true; do
    state=false
      echo "New state reached: $state"
      echo "Publishing new results"
      curl -v -X PUT http://"$endpoint""$propid" -H 'Content-Type: text/occi'  -H 'Category: monitorableproperty; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="'$sensor'",occi.core.target="'$propresource'", occi.core.title="monProp", monitoring.result="'$state'", monitoring.property="'$propname'"' 
    sleep $sequence
done

