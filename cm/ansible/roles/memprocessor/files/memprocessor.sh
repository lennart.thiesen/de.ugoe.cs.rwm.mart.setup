#! /bin/bash

avgMem=0
slept=0
sequence=3
times=3
send=$(python -c "print $sequence*$times")
url=http://$1:61208/api/2/mem/percent

if [ -f ~/memmonitor.txt ]; then
    echo "Removing previous monitoring data"
    rm monitor.txt
fi

echo "Starting monitoring script"
echo "Requesting $url every $sequence seconds aggregating after $times times!"


while true; do
    mem=$(curl -s $url | awk '{print $2}')
    cutmem=$(echo "$mem" | rev | cut -c 2- | rev)
    sumMem=$(python -c "print $sumMem+$cutmem")
    echo "Current Mem: $cutmem"
    sleep $sequence
    slept=$(($slept+$sequence))
    if [ "$slept" = $send ]; then
       mid=$(python -c "print $sumMem/$times")
       echo "Aggregate Mem: $mid"
       echo $mid >> ~/memmonitor.txt
       slept=0
       sumMem=0
    fi
done
