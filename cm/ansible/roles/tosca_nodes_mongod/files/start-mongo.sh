#!/bin/bash

echo "BEGIN mongo start"

echo "service mongo start"

sudo service mongodb start

echo "END service mongo stop"


sudo echo "line use to check if mongo is restarting, do not delete it" | sudo tee /var/log/mongodb/mongodb.log

COUNTER=0
mongostart=''
while [  $COUNTER -lt 30 ]; do
   let COUNTER=COUNTER+1
   echo "waiting for mongo to start: try $COUNTER /30"
   mongostart=$(sudo tail -1 /var/log/mongodb/mongodb.log | grep "waiting for connections on port")
   if [[ ! -z $mongostart ]]
   then
       echo "END mongo service start"
       exit 0
   fi
   sleep 5
done

echo "ERROR: timeout fail to start mongo"
exit 1
