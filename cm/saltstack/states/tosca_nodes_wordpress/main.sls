undeploy:
   pkg.installed:
     - pkgs:
       - rsync
       - lftp
       - curl
   file.directory:
     - name: /opt/my_old_directory
     - user: root
     - group: root
     - mode: 755

deploy:
   pkg.installed:
     - pkgs:
       - rsync
       - lftp
       - curl
   file.directory:
     - name: /opt/my_old_directory
     - user: root
     - group: root
     - mode: 755

