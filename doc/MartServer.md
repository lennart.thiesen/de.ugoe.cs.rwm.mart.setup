# MartServer Cloud Configuration
The [MartServer](https://github.com/occiware/MartServer) represents the OCCI interface to which requests are send in order to provision and deploy cloud applications.
This server is easy to extend by plugging in connectors created for modeled OCCI extensions. In the following you find an example setup we used to deploy the MartServer in the cloud, as well as plugin configurations managing incoming OCCI requests. As our studies are performed on a private [Openstack](https://www.openstack.org/) cloud (Version: [Newton](https://releases.openstack.org/newton/)) our description focuses on this environment. However, a similar deployment setup can be used within any cloud environment. The figure below demonstrates the cloud state to be reached.

<img src="./Martoverview.jpg" width="400">

## Deploying the MartServer in the cloud
First you need to setup a Virtual Machine hosting the MartServer:
1. Start a Virtual Machine, e.g., with an Ubuntu 16.04 image
2. Login to your Virtual Machine and register the API of your Cloud provider
    1. For example: In case of an OpenStack cloud insert the OpenStack Controller IP in /etc/hosts (192.168.34.1 controller).
3. Install every other software required by the MartServer setup on the machine running the Server. In our case this comprises:
    1. [Java](https://www.java.com/en/), Version 1.8
    2. [Ansible](https://docs.ansible.com/), Version 2.7.2
    3. **Optional:** [Saltstack](https://www.saltstack.com/), Salt-SSH Version 2019.2.0 (Fluorine)
4. Deploy the MartServer application
   1. Clone this repository on the target machine
   2. Alternatively, check out the [MartServer project](https://github.com/occiware/MartServer) for the latest version.

*Note:* The MartServer fat jar provided within this repository was generated from the version defined within this [repository](https://github.com/kortef/MartServer). Within this repository dependencies of the MartServer are updated to cope with the capabilities provided by the [MoDMaCAO](https://github.com/occiware/MoDMaCAO) framework. To generate the fat jar the pom.xml of the org.occiware.mart.jetty project enhanced with the [maven assembly plugin](https://www.mkyong.com/maven/create-a-fat-jar-file-maven-assembly-plugin/).

### MartServer management network configuration
To execute configuration management scripts using the MartServer a connection to each provisioned Virtual Machine is required. 
Therefore, we create a management network to which the MartServer is connected, as well as each new Virtual Machine. A figure depicting the general setup is shown below.
The steps required to reach this configuration are described in the following.
1. Create a network within your cloud environment, e.g., by using its web interface
   1. E.g. in OpenStack:
      1. Name the network ManagmenetNetwork
      2. Name the subnet management-subnet
      3. Set the address to 10.0.0.0/24
      4. Enable DHCP and define a DNS server (e.g. 8.8.8.8)
2. Attach the Virtual Machine that hosts the MartServer to the network
3. Configure the created interface on the VirtualMachine
   1. E.g. in Ubuntu 16.04 Cloud image:
        1. Find out the name of the new interface (ifconfig -a)
        2. Add the new interface to your /etc/network/interfaces configuration
        3. Start the interface (ifup $interfacename)
        4. Ping to your management network to test the setup 
4. Create a router that connects the public to the management network
5. Startup the MartServer and SmartWYRM as [previously described](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup)
6. Register your management network in the MartServer runtime model:
         1. Identify the runtime id of the created management network
         2. Perform a request to register the network in the OCCI runtime model (see below)
7. Configure the SmartWYRM network settings
```
curl -v -X PUT $OCCI_SERVER_URL:8080/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590 -H 'Content-Type: text/occi' -H 'Category: network; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", runtimeid; scheme="http://schemas.modmacao.org/openstack/runtime#"; class="mixin";' -H 'X-OCCI-Attribute: occi.core.id="urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590"' -H 'X-OCCI-Attribute: occi.core.title="Network", occi.network.state="active", openstack.runtime.id="$MANAGEMENT_NETWORK_RUNTIME_ID"'
```

*Note:* Do not forget to correctly setup your firewall rules.

## Configuring the MartServer plugins
Before something can be deployed in the cloud some connectors have to be configured to gain access to specific information, e.g., your openstack credentials or other user specific information.
1. If the MartServer is running stop it
2. Navigate to the martserver-plugins folder of this repository
3. Merge the plugins contained within the [martserver-plugins/live](../martserver-plugins/live) folder with the ones in the [martserver-plugins](../martserver-plugins) folder
4. Delete all unneccessary "dummy" connector plugins (wocci, mocci, cm, infrastructure)
5. Configure the plugins as described in the guide found [here](../martserver-plugins/)